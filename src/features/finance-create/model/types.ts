export interface IExpenseCreate {
  categoryId: number
  title: string
  amount: string
  date: Date
}

export interface IIncomeCreate {
  title: string
  amount: string
  date: Date
}