// store of all expenses
import { router } from '@/app/providers'
import { AppRoutesPaths } from '@/pages'
import { useFinancesStore } from '@/widgets/finances'
import { LOCAL_STORAGE_USER_KEY } from '@/shared/constants'
import { apiInstance } from '@/shared/api'
import type { IExpenseCreate, IIncomeCreate } from './types'
import type { IExpense, IIncome } from '@/widgets/finances/model/types'
import { AxiosError, isAxiosError } from 'axios' // eslint-disable-line

// api post rhen response get created finance and push to store

export async function createFinance(finance: IExpenseCreate | IIncomeCreate, isIncome: boolean) {
  const store = useFinancesStore()
  type IFinanceReponse = typeof isIncome extends true ? IIncome : IExpense
  const financeString = isIncome ? 'incomes' : 'expenses'

  const user = localStorage.getItem(LOCAL_STORAGE_USER_KEY)
  if (!user) router.push(AppRoutesPaths.auth)
  const userId = user && JSON.parse(user).id
  const newFinance = {
    userId,
    ...finance
  }
  try {
    const { data: createdFinance } = await apiInstance.post<IFinanceReponse>(`/${financeString}`, newFinance)
    const dateKey = new Date(createdFinance.date).toISOString().split('T')[0]
    if (store[financeString][dateKey]) {
      store[financeString][dateKey].unshift(createdFinance)
    } else {
      store[financeString] = Object.assign({ [dateKey]: [createdFinance] }, store[financeString])
    }
  } catch (e) {
    if (isAxiosError(e) && e.response) {
      store.error = e.response.data.message
    } else {
      store.error = 'Произошла непредвиденная ошибка'
    }
  }
}
