import { router } from '@/app/providers'
import { apiInstance } from '@/shared/api'
import { LOCAL_STORAGE_TOKEN_KEY, LOCAL_STORAGE_USER_KEY } from '@/shared/constants'
import { AxiosError, isAxiosError } from 'axios' // eslint-disable-line
import { defineStore } from 'pinia'
import { ref } from 'vue'
import type { AuthResponse } from './types'
import { AppRoutesPaths } from '@/pages'

export const useAuthStore = defineStore('auth', () => {
  const isLoading = ref<boolean>(false)
  const error = ref<string>('')
  const isAuthenticated = ref<boolean>(false)
  const isCreateAccountModalOpen = ref<boolean>(false)

  async function auth(email: string, password: string, type: 'signin' | 'signup' | 'test') {
    isLoading.value = true
    try {
      const localStorageUser = localStorage.getItem(LOCAL_STORAGE_USER_KEY)
      const user = localStorageUser && JSON.parse(localStorageUser)
      const { data } = await apiInstance.post<AuthResponse>(`/auth/${type}`, {
        ...(user?.isTest && { id: user.id, isTest: user.isTest }),
        email,
        password
      })
      isAuthenticated.value = true
      localStorage.setItem(LOCAL_STORAGE_TOKEN_KEY, data.accessToken)
      localStorage.setItem(LOCAL_STORAGE_USER_KEY, JSON.stringify(data.user))
      error.value = ''
      isCreateAccountModalOpen.value = false
      router.push(AppRoutesPaths.finances)
    } catch (e) {
      isAuthenticated.value = false
      if (isAxiosError(e) && e.response) {
        error.value = e.response.data.message
      } else {
        error.value = 'Произошла непредвиденная ошибка'
      }
    } finally {
      isLoading.value = false
    }
  }

  async function verify() {
    try {
      await apiInstance.get('/auth/verify')
    } catch (e) {
      //  && e.response && e.response.status === (401 || 403)
      if (isAxiosError(e)) {
        localStorage.removeItem(LOCAL_STORAGE_USER_KEY)
        router.push(AppRoutesPaths.auth)
      }
    }
  }

  async function signOut() {
    const localStorageUser = localStorage.getItem(LOCAL_STORAGE_USER_KEY)
    const user = localStorageUser && JSON.parse(localStorageUser)
    if (user.isTest) {
      await apiInstance.post('/auth/remove', { id: user.id })
    }
    apiInstance.post('/auth/signout')
    localStorage.removeItem(LOCAL_STORAGE_TOKEN_KEY)
    localStorage.removeItem(LOCAL_STORAGE_USER_KEY)
    router.push(AppRoutesPaths.auth)
  }

  return { isLoading, error, auth, verify, signOut, isCreateAccountModalOpen }
})
