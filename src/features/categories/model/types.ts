export type ICategory = {
  id: number
  title: string
  svg: string
}

export interface SelectorProps {
  chosen: ICategory | null
}
