import { apiInstance } from '@/shared/api'
import { AxiosError, isAxiosError } from 'axios' // eslint-disable-line
import { defineStore } from 'pinia'
import { ref } from 'vue'
import type { ICategory } from './types'

export const useCategoriesStore = defineStore('categories', () => {
  const isLoading = ref<boolean>(false)
  const error = ref<string>('')
  const categories = ref<ICategory[]>([])

  async function fetchCategories() {
    isLoading.value = true
    try {
      const { data: categoriesData } = await apiInstance.get<ICategory[]>('/categories')
      categories.value = categoriesData
    } catch (e) {
      if (isAxiosError(e) && e.response) {
        error.value = e.response.data.message
      } else {
        error.value = 'Произошла непредвиденная ошибка'
      }
    } finally {
      isLoading.value = false
    }
  }

  return { isLoading, error, categories, fetchCategories }
})
