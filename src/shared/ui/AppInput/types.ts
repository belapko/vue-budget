type InputTheme = 'underlined' | 'outlined'

export interface Props {
  type?: string
  theme?: InputTheme
  label?: string
  value?: string
  autofocus?: boolean
  placeholder?: string
}
