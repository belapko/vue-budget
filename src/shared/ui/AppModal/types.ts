export interface Props {
  title?: string
  isOpen: boolean
  onClose: () => void
  maxWidth?: string
}
