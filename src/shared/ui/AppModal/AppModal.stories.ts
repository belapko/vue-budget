import type { Meta, StoryObj } from '@storybook/vue3'

import AppModal from './AppModal.vue'

// More on how to set up stories at: https://storybook.js.org/docs/vue/writing-stories/introduction
const meta = {
  title: 'shared/AppModal',
  component: AppModal,
  // This component will have an automatically generated docsPage entry: https://storybook.js.org/docs/vue/writing-docs/autodocs
  tags: ['autodocs']
} satisfies Meta<typeof AppModal>

export default meta
type Story = StoryObj<typeof AppModal>
/*
 *👇 Render functions are a framework specific feature to allow you control on how the component renders.
 * See https://storybook.js.org/docs/vue/api/csf
 * to learn how to use render functions.
 */
export const Modal: Story = {
  render: (args) => ({
    components: { AppModal },
    setup() {
      return { args }
    },
    template:
      '<AppModal v-bind="args"><p>Наполнение модалки</p><p>Модалка может быть ограничена по максимальной ширине</p><p>Модалка может быть без заголовка</p></AppModal>'
  }),
  args: {
    isOpen: true,
    title: 'Modal Title'
  }
}
