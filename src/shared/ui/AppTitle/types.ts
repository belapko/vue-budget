type TitleSize = 'small' | 'medium' | 'large'

export interface Props {
  size?: TitleSize
}
