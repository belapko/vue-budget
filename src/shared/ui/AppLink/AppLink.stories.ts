import type { Meta, StoryObj } from '@storybook/vue3'

import AppLink from './AppLink.vue'

// More on how to set up stories at: https://storybook.js.org/docs/vue/writing-stories/introduction
const meta = {
  title: 'shared/AppLink',
  component: AppLink,
  // This component will have an automatically generated docsPage entry: https://storybook.js.org/docs/vue/writing-docs/autodocs
  tags: ['autodocs']
} satisfies Meta<typeof AppLink>

export default meta
type Story = StoryObj<typeof AppLink>
/*
 *👇 Render functions are a framework specific feature to allow you control on how the component renders.
 * See https://storybook.js.org/docs/vue/api/csf
 * to learn how to use render functions.
 */
export const Anchor: Story = {
  render: (args) => ({
    components: { AppLink },
    setup() {
      return { args }
    },
    template: '<AppLink v-bind="args"/>'
  }),
  args: {
    text: 'anchor link',
    to: 'https://github.com/belapko'
  }
}

export const RouterLink: Story = {
  render: (args) => ({
    components: { AppLink },
    setup() {
      return { args }
    },
    template: '<AppLink v-bind="args"/>'
  }),
  args: {
    text: 'router link',
    routerLink: true,
    to: '/app'
  }
}
