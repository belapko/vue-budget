type LinkTheme = 'default' | 'clear'

export interface Props {
  routerLink?: boolean
  theme?: LinkTheme
  to: string
  text: string
}
