import type { RouteRecordRaw } from 'vue-router'

export enum AppRoutes {
  AUTH = 'auth',
  FINANCES = 'finances',
  SETTINGS = 'settings'
}

export const AppRoutesPaths: Record<AppRoutes, string> = {
  [AppRoutes.AUTH]: '/auth',
  [AppRoutes.FINANCES]: '/finances',
  [AppRoutes.SETTINGS]: '/settings'
}

export const routes: Array<RouteRecordRaw> = [
  {
    path: AppRoutesPaths.auth,
    name: AppRoutes.AUTH,
    component: () => import('./authentication/ui/AuthenticationPage.vue')
  },
  {
    path: AppRoutesPaths.finances,
    name: AppRoutes.FINANCES,
    component: () => import('./finances/ui/FinancesPage.vue')
  },
  {
    path: AppRoutesPaths.settings,
    name: AppRoutes.SETTINGS,
    component: () => import('./settings/ui/SettingsPage.vue')
  },
]
