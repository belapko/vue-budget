import { useFinancesStore } from '@/widgets/finances'
import type { IExpense, IIncome } from '@/widgets/finances/model/types'
import { apiInstance } from '@/shared/api'
import { LOCAL_STORAGE_USER_KEY } from '@/shared/constants'
import { AxiosError, isAxiosError } from 'axios' // eslint-disable-line

export async function removeFinance(financeId: number, isIncome: boolean) {
  const store = useFinancesStore()
  try {
    const financeString = isIncome ? 'incomes' : 'expenses'

    const user = localStorage.getItem(LOCAL_STORAGE_USER_KEY)
    const userId = user && JSON.parse(user).id
    await apiInstance.delete(`/${financeString}`, { data: { financeId, userId } })
    let newArray
    for (const [key, value] of Object.entries(store[financeString])) {
      if (value.find((finance) => finance.id === financeId)) {
        if (value.length === 1) {
          delete store[financeString][key]
        } else {
          newArray = value.filter((finance) => finance.id !== financeId)
          store[financeString][key] = newArray
        }
        return
      }
    }
  } catch (e) {
    if (isAxiosError(e) && e.response) {
      store.error = e.response.data.message
    } else {
      store.error = 'Произошла непредвиденная ошибка'
    }
  }
}

export async function updateFinance(changedFinance: Partial<IExpense> | Partial<IIncome>, isIncome: boolean) {
  const store = useFinancesStore()
  try {
    type IFinanceReponse = typeof isIncome extends true ? IIncome : IExpense
    const financeString = isIncome ? 'incomes' : 'expenses'
    const user = localStorage.getItem(LOCAL_STORAGE_USER_KEY)
    const userId = user && JSON.parse(user).id
    const { data: updatedFinance } = await apiInstance.put<IFinanceReponse>(`/${financeString}`, { finance: changedFinance, userId })
    for (const [key, value] of Object.entries(store[financeString])) {
      if (value.find((finance) => finance.id === updatedFinance.id)) {
        store[financeString][key] = value.map(finance => finance.id !== updatedFinance.id ? finance : updatedFinance)
        return
      }
    }
  } catch (e) {
    if (isAxiosError(e) && e.response) {
      store.error = e.response.data.message
    } else {
      store.error = 'Произошла непредвиденная ошибка'
    }
  }
}
