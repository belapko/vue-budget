import { defineStore } from 'pinia'
import { ref } from 'vue'
import type { IExpense, IIncome, IGroupedFinances } from './types'
import { apiInstance } from '@/shared/api'
import { AxiosError, isAxiosError } from 'axios' // eslint-disable-line
import { LOCAL_STORAGE_USER_KEY } from '@/shared/constants'

interface IFetch {
  page: number
  limit: number
  categories?: number[]
  fromDate?: Date | null
  toDate?: Date | null
  isIncomes?: boolean
}

export const useFinancesStore = defineStore('finances', () => {
  const isLoading = ref<boolean>(false)
  const error = ref<null | string>(null)
  const expenses = ref<IGroupedFinances<IExpense>>({})
  const incomes = ref<IGroupedFinances<IIncome>>({})

  async function fetchFinances({ page, limit, categories, fromDate, toDate, isIncomes }: IFetch) {
    try {
      // console.log(page, limit, categoryId, fromDate, toDate)
      const endpointString = isIncomes ? 'incomes' : 'expenses'
      const userId = JSON.parse(localStorage.getItem(LOCAL_STORAGE_USER_KEY) ?? '').id
      isLoading.value = true
      const requestString: string =
        (fromDate && toDate) || categories?.length
          ? `/${endpointString}/${userId}?from=${fromDate}&to=${toDate}&categories=${categories?.join(',')}`
          : `/${endpointString}/${userId}`
      const response = await apiInstance.get(requestString) // ?page=${page}&limit=${limit}&categoryId=${categoryId}`)
      if (isIncomes) {
        incomes.value = response.data
      } else {
        expenses.value = response.data
      }
    } catch (e) {
      if (isAxiosError(e) && e.response) {
        error.value = e.response.data.message
      } else {
        error.value = 'Произошла непредвиденная ошибка'
      }
      alert(error.value)
    } finally {
      isLoading.value = false
    }
  }

  return { isLoading, error, expenses, incomes, fetchFinances }
})
