export interface IExpense {
  id: number
  title: string
  amount: string
  date: Date
  userId: number
  categoryId: number
  category: {
    id: number
    title: string
    svg: string
  }
}

export interface IIncome {
  id: number
  title: string
  amount: string
  date: Date
  userId: number
}

export interface IGroupedFinances<T> {
  [date: string]: T[]
}
