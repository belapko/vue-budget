import AppFinances from './ui/AppFinances.vue'
import { useFinancesStore } from './model'

export { AppFinances, useFinancesStore }
