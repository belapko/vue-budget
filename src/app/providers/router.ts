import { createRouter, createWebHistory } from 'vue-router'
import { AppRoutes, AppRoutesPaths, routes } from '@/pages'
import { LOCAL_STORAGE_USER_KEY } from '@/shared/constants'

const router = createRouter({
  history: createWebHistory(import.meta.env.BASE_URL),
  routes
})

// if user not authenticated redirect to auth
router.beforeEach((to) => {
  if (to.name !== AppRoutes.AUTH && !localStorage.getItem(LOCAL_STORAGE_USER_KEY)) {
    return { name: AppRoutes.AUTH }
  }
})

// if page not exists or user not authenticated
router.beforeEach((to, from) => {
  if (!to.matched.length) {
    const isBackExists = router.back()
    if (isBackExists !== undefined && from.path === '/') router.back()
    else if (!localStorage.getItem(LOCAL_STORAGE_USER_KEY)) router.push(AppRoutesPaths.auth)
    else router.push(AppRoutesPaths.finances)
  }
})

export { router }
